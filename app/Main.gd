extends Node

export (PackedScene) var mobScene
var MOB_SCENE = mobScene as Node2D
export var muteAudio = false
export var mobSpawnWaitTime = 10
export var mobWaitRandomize = 0.9
export var mobShootWaitTime = 5
export var mobShootWaitRandomize = 0.5
var screenSize = Vector2.ZERO
var score = 0
var LEVEL = 1
var levelIncreaseDenominator = 10
var pooTimerDecreasePerLevel = 0.1
var maxPoints = 10
var numMisses = 0
var maxMisses = 10
var mobs = []
var missedShite = []
var missSounds = []
var isPlaying = false
onready var HUD = $CanvasLayer/HUD


# Called when the node enters the scene tree for the first time.
func _ready():
	# Mute audio
	AudioServer.set_bus_mute(AudioServer.get_bus_index("Master"), muteAudio)
	screenSize = get_viewport().size
	HUD.showStartScreen()
	missSounds.append(preload("res://audio/splat1.wav"))
	missSounds.append(preload("res://audio/splat2.wav"))
	missSounds.append(preload("res://audio/splat3.wav"))
	missSounds.append(preload("res://audio/splat4.wav"))
	missSounds.append(preload("res://audio/splat5.wav"))
	# Set player bounds
	$Player.moveBounds = $PlayerMoveBounds.get_global_rect()
	resetUI()


func resetUI():
	HUD.updateMisses(0, maxMisses)
	randomizeMobSpawn()
	setScore(0)
	LEVEL = 1
	HUD.setLevel(LEVEL)


func pauseGame():
	get_tree().paused = not get_tree().paused
	HUD.togglePauseScreen(get_tree().paused)


func resumeGame():
	get_tree().paused = not get_tree().paused
	HUD.hidePauseScreen()


func startGame():
	# Remove all mobs and shit
	for node in get_tree().root.get_children():
		if node.is_in_group("shit") or node.is_in_group("mob"):
			node.queue_free()
		else:
			print(node)
	randomize()
	isPlaying = true
	$Music.play()
	numMisses = 0
	mobs = []
	missedShite = []
	resetUI()
	HUD.hideScreens()
	$MobTimer.start()
	spawnMob()


func setScore(value):
	score = value
	HUD.updateScore(value, maxPoints)


func setMisses(value):
	numMisses = value
	HUD.updateMisses(numMisses, maxMisses)


func gainPoints(value):
	if not isPlaying:
		return
	setScore(score + value)
	var level = 1 + floor(score / levelIncreaseDenominator)
	if level != LEVEL:
		LEVEL = level
		HUD.setLevel(LEVEL)


func takeDamage(value):
	if not isPlaying:
		return
	setMisses(numMisses + value)
	if numMisses >= maxMisses:
		gameOver()


func gameOver():
	isPlaying = false
	$Music.stop()
	HUD.showGameOverScreen()
	$MobTimer.stop()
	for mob in mobs:
		mob.pause()


func spawnMob():
	"""
	This function determines most of the difficulty of the game
	It sets ensures that the distance between spawned enemies starts small
	and gets linearly larger as the player progresses.
	"""
	var spawnLocation = $MobPath/MobSpawnLocation
	var maxMobs = 6
	if len(mobs) >= maxMobs:
		# Don't spawn more mobs if max count reached
		return
	# Spawn on the side of the mob that has the most space between itself and the curve end
	var mobDistance = 1.0 / float(maxMobs)
	spawnLocation.unit_offset = 1.0
	var curveLength = float(spawnLocation.offset)
	var mobUnitOffsets = []
	for mob in mobs:
		var offset = $MobPath.curve.get_closest_offset(mob.position)
		var unitOffset = float(offset) / curveLength
		mobUnitOffsets.append(unitOffset)
	mobUnitOffsets.sort()
	if not mobUnitOffsets:
		mobUnitOffsets.append(randf())
	mobUnitOffsets.insert(0, 0.0)
	mobUnitOffsets.append(1.0)
	var gapEdges = [0.0, 1.0]
	var largestGap = 0.0
	for index in range(len(mobUnitOffsets)):
		var leftOffset = mobUnitOffsets[index]
		var rightOffset = mobUnitOffsets.back()
		if index + 1 < len(mobUnitOffsets) - 1:
			rightOffset = mobUnitOffsets[index + 1]
		var gap = rightOffset - leftOffset
		if gap > largestGap:
			largestGap = gap
			gapEdges = [leftOffset, rightOffset]
	# Place new mob in biggest gap on either left/right side
	var leftOffset = min(gapEdges[0], gapEdges[1])
	var rightOffset = max(gapEdges[0], gapEdges[1])
	spawnLocation.unit_offset = leftOffset + ((rightOffset - leftOffset) / 2)
	# Instantiate a new mob and place it relative to the others on screen
	var mob = mobScene.instance()
	mob.shootWaitTime = mobShootWaitTime
	mob.shootTimeRandomize = mobShootWaitRandomize
	get_tree().root.add_child(mob)
	var initPos = Vector2(spawnLocation.position)
	initPos.y = $MobPath.position.y
	mob.spawn(initPos)
	mobs.append(mob)
	# Despawn a mob if there are too many of the field
	var maxSimultaneousMobs = min(LEVEL + 1, maxMobs)
	if len(mobs) > maxSimultaneousMobs:
		var randInd = int(rand_range(0.0, len(mobs) - 1))
		mobs.pop_at(randInd).deSpawn()
	randomizeMobSpawn()


func randomizeMobSpawn():
	var mult = mobSpawnWaitTime / 2
	var offset = rand_range(-mult, mult)
	$MobTimer.wait_time = mobSpawnWaitTime + offset
	print("wait time: %f, offset: %f" % [$MobTimer.wait_time, offset])


#====================
# CALLBACKS
#====================
func _on_MobTimer_timeout():
	spawnMob()


func _on_Player_flushed(poops):
	gainPoints(len(poops))


func _on_HUD_startPressed():
	startGame()


func _on_Floor_gotHit(body):
	missSounds.shuffle()
	$Miss.stream=missSounds.front()
	$Miss.play()
	if body in missedShite:
		return
	missedShite.append(body)
	takeDamage(body.damage)


func _on_HUD_resumePressed():
	resumeGame()


func _on_HUD_menuPressed():
	if not isPlaying:
		return
	pauseGame()
