extends Node2D


export (PackedScene) var Projectile
onready var TweenSpawn = $TweenSpawn
onready var TweenPush = $TweenPush
onready var Muzzle = $Muzzle
onready var PooTimer = $PooTimer
export var speed = 100
export var shootWaitTime = 5
export var shootTimeRandomize = 0.5
var screenSize = Vector2.ZERO
var direction = Vector2.RIGHT
var move = false


# Called when the node enters the scene tree for the first time.
func _ready():
	position.y = -50
	screenSize = get_viewport_rect().size
	randomizeWaitTime()


func _process(delta):
	if not move:
		return
	var value = speed * delta
	# Change direction at screen edge
	if position.x >= screenSize.x:
		direction = Vector2.LEFT
	if position.x <= 0:
		direction = Vector2.RIGHT
	position.x += value * direction.x


func spawn(position):
	var initPos = Vector2(position - Vector2(0, 50))
	var endPos = position
	position = initPos
	TweenSpawn.interpolate_property(self, "position",
		initPos, endPos,
		1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	TweenSpawn.start()


func deSpawn():
	pause()
	var initPos = Vector2(position)
	var endPos = Vector2(initPos - Vector2(0, 50))
	TweenSpawn.interpolate_property(self, "position", initPos, endPos, 1.0, Tween.TRANS_ELASTIC, Tween.EASE_IN)
	TweenSpawn.start()


func shoot():
	if PooTimer.paused:
		return
	var poop = Projectile.instance()
	get_tree().root.add_child(poop)
	var startPos = Muzzle.global_position
	var endPos = Vector2(startPos.x, startPos.y)
	endPos.y += 50
	# Turn off gravity so poop position can be animated manually
	var gravityScale = poop.gravity_scale
	poop.gravity_scale = 0.0
	poop.global_position = startPos
	TweenPush.interpolate_property(poop, "position", startPos, endPos, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	TweenPush.interpolate_property(poop, "gravity_scale", 0.0, 1.0, 1.0, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	# Wait for first animation to complete
	TweenPush.start()
	yield(TweenPush, "tween_completed")
	$Fart.pitch_scale = 1 + (randf() - 0.5 )
	$Fart.play()


func pause():
	move = false
	PooTimer.paused = true


func randomizeWaitTime():
	# Randomize waitTime by half the baseTime so that
	#   some shots come earlier and some later that the first
	var mult = shootWaitTime * shootTimeRandomize
	var offset = rand_range(-mult, mult)
	PooTimer.wait_time = shootWaitTime + offset
	print("wait time: %f, offset: %f" % [PooTimer.wait_time, offset])

func _on_PooTimer_timeout():
	shoot()
	randomizeWaitTime()


func _on_Tween_tween_completed(object, key):
	shoot()
	PooTimer.start(shootWaitTime)
