extends Control

var SCORE_FILE_PATH = "user://highscore.txt"
var SCORE = 0
signal okPressed
onready var nameEdit = $SaveBox/VBoxContainer/nameEdit
onready var saveBox = $SaveBox
onready var scoreBox = $ScoreBox
onready var scoreList = $ScoreBox/VBoxContainer/ScoreList

# Called when the node enters the scene tree for the first time.
func _ready():
	populateScoreList()


func setup(points):
	if points <= 0:
		saveBox.visible = false
		return
	SCORE = points
	saveBox.visible = true


func loadScoresFromFile():
	var scores = []
	var f = File.new()
	if f.file_exists(SCORE_FILE_PATH):
		f.open(SCORE_FILE_PATH, File.READ)
		var content = f.get_as_text()
		for line in content.split("\n"):
			# Skip empty lines
			if not line:
				continue
			scores.append(line)
		f.close()
	# Sort alphabetically
	scores.sort()
	scores.invert()
	return scores


func saveScoreToFile(numPoints, playerName):
	var f = File.new()
	var scores = loadScoresFromFile()
	var formatScore = "%d %s" % [numPoints, playerName]
	scores.append(formatScore)
	f.open(SCORE_FILE_PATH, File.WRITE)
	for score in scores:
		f.store_line(score)
	f.close()


func populateScoreList():
	var scores = loadScoresFromFile()
	scoreList.clear()
	for score in scores:
		scoreList.add_item(score)


func _on_nameEdit_text_changed(new_text):
	var regEx = RegEx.new()
	regEx.compile("[^a-zA-Z0-9\\-]")
	var cleanText = regEx.sub(new_text, "-", true)
	var cPos = nameEdit.caret_position
	nameEdit.text = cleanText
	nameEdit.caret_position = cPos


func _on_saveButton_pressed():
	saveBox.visible = false
	saveScoreToFile(SCORE, nameEdit.text)
	populateScoreList()


func _on_okButton_pressed():
	emit_signal("okPressed")
