extends KinematicBody2D

var speed = 250.0
var screenSize = Vector2.ZERO
export var moveBounds = Rect2(0.0, 0.0, 1000.0, 1000.0)  # Set via level script
onready var TweenNode = $Tween
var playerWidth = 1.0
signal flushed


# Called when the node enters the scene tree for the first time.
func _ready():
	screenSize = get_viewport_rect().size
	playerWidth = $Front.texture.get_width()


func start(new_position):
	position = new_position
	show()


func moveWithKeys(delta):
	# MOVE
	var direction = Vector2.ZERO
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
	if direction.length() > 0:
		# If 2 keys are pressed, make sure they don't move faster
		direction = direction.normalized()
	if Input.is_action_pressed("ui_boost"):
		direction.x *= 2
	position += direction * speed * delta
	position.x = clamp(position.x, 0, screenSize.x)
	position.y = clamp(position.y, 0, screenSize.y)


export var moveEaseSpeed = 0.33

func moveWithMouse(delta):
	var mousePos = get_global_mouse_position()
	var pos = Vector2(position)
	var bounds = moveBounds
	pos.x = clamp(mousePos.x, moveBounds.position.x + playerWidth / 2, moveBounds.end.x - playerWidth / 2)
	# Moving the player directly allows for simple easing. However they won't
	# interact with other physics objects properly but may move through them.
	# To overcome this, we'll apply force
	# A) Apply force
	# B) EASE animation
	TweenNode.interpolate_property(self, "position:x",
		null, pos.x,
		moveEaseSpeed, Tween.TRANS_EXPO, Tween.EASE_OUT)
	TweenNode.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Call one of the moveWith* function
	# moveWithKeys(delta)
	moveWithMouse(delta)


#Jump 
export var fall_gravity_scale := 150.0
export var low_jump_gravity_scale := 100.0
export var jump_power := 500.0
var jump_released = false

#Physics
var velocity = Vector2()
var earth_gravity = 9.807 # m/s^2
export var gravity_scale := 100.0
var on_floor = false


func _physics_process(delta):
	if Input.is_action_just_released("ui_up"):
		jump_released = true

	#Applying gravity to player
	velocity += Vector2.DOWN * earth_gravity * gravity_scale * delta

	#Jump Physics
	if velocity.y > 0: #Player is falling
		#Falling action is faster than jumping action | Like in mario
		#On falling we apply a second gravity to the player
		#We apply ((gravity_scale + fall_gravity_scale) * earth_gravity) gravity on the player
		velocity += Vector2.DOWN * earth_gravity * fall_gravity_scale * delta 

	elif velocity.y < 0 && jump_released: #Player is jumping 
		#Jump Height depends on how long you will hold key
		#If we release the jump before reaching the max height 
		#We apply ((gravity_scale + low_jump_gravity_scale) * earth_gravity) gravity on the player
		#It result on a lower jump
		velocity += Vector2.DOWN * earth_gravity * low_jump_gravity_scale * delta

	if on_floor:
		if Input.is_action_just_pressed("ui_up"):
			velocity = Vector2.UP * jump_power #Normal Jump action
			jump_released = false

	velocity = move_and_slide(velocity, Vector2.UP, false, 4, PI / 4, true)
	on_floor = is_on_floor()


func _on_Hole_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("shit"):
		# Randomize splash pitch
		$SplashSound.pitch_scale = 1 + (randf() - 0.5 )
		$SplashSound.play()
		# Remove shit
		body.set_deferred("disabled", true)
		body.hide()
		body.queue_free()
		emit_signal("flushed", [body])
