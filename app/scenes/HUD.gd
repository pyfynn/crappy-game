extends Control


var SCORE = 0
signal startPressed
signal resumePressed
signal menuPressed
onready var HIGH_SCORE_SCREEN = $HighScoreScreen


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		emit_signal("menuPressed")


func updateProgress(value, maxValue, progressBarObject, labelObject):
	print("updating %s from %s > %s" % [progressBarObject.name, value, maxValue])
	labelObject.text = str("%d/%d" % [value, maxValue])
	var percent = float(value) / float(maxValue) * 100.0
	if value == 0:
		progressBarObject.value = value
		return
	var tween = $Tween
	tween.interpolate_property(progressBarObject, "value", null, percent, 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()
	# progressBarObject.value = newValue


func wiggleNode(node):
	var tween = $Tween
	node.scale = Vector2(1, 1)
	tween.interpolate_property(node, "scale", null, Vector2(1.5, 1.5), 0.15, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.start()
	# Wait for first animation to complete
	yield(tween, "tween_completed")
	tween.interpolate_property(node, "scale", null, Vector2(1, 1), 1.0, Tween.TRANS_ELASTIC, Tween.EASE_OUT)


func updateScore(points, maxPoints):
	# updateProgress(points, maxPoints, $StatusBar/progressPoints, $StatusBar/progressPoints/ScoreLabel)
	SCORE = points
	$StatusBar/ScoreLabel.text = "{points}".format({"points": points})
	wiggleNode($pooPointsIcon)


func setLevel(level):
	var levelLabel = $LevelLabel
	levelLabel.text = "%d" % level
	wiggleNode($LvLGroup)


func updateMisses(misses, maxMisses):
	updateProgress(misses, maxMisses, $StatusBar/progressMisses, $StatusBar/progressMisses/MissesLabel)


func toggleStartScreen(state):
	$StartScreen.visible = state


func toggleHighScoreScreen(state):
	HIGH_SCORE_SCREEN.visible = state


func togglePauseScreen(state):
	$PauseScreen.visible = state


func showPauseScreen():
	hideScreens()
	togglePauseScreen(true)


func hidePauseScreen():
	togglePauseScreen(false)


func showStartScreen():
	hideScreens()
	toggleStartScreen(true)


func showHighScoreScreen():
	hideScreens()
	HIGH_SCORE_SCREEN.setup(SCORE)
	toggleHighScoreScreen(true)
	

func hideScreens():
	toggleHighScoreScreen(false)
	toggleGameOverScreen(false)
	toggleStartScreen(false)
	togglePauseScreen(false)


func toggleGameOverScreen(state):
	$GameOverScreen.visible = state


func showGameOverScreen():
	hideScreens()
	toggleGameOverScreen(true)


func _on_StartScreen_buttonPressed():
	emit_signal("startPressed")


func _on_GameOverScreen_buttonPressed():
	showHighScoreScreen()


func _on_HighScoreScreen_okPressed():
	hideScreens()
	showStartScreen()


func _on_PauseScreen_buttonPressed():
	hidePauseScreen()
	emit_signal("resumePressed")


func _on_MenuButton_pressed():
	emit_signal("menuPressed")
