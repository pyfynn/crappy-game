extends Node2D

export (PackedScene) var Projectile

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var CURRENT_TIME: float = 0.0
var MOUSE_IN_ARENA = true
var VIEWPORT_SIZE = Vector2.ZERO
var FIRE_RATE: float = 5.0
var POO_MASS: float = 0.0
var GRAVITY: float = 98
onready var massSlider = $UI/VBoxContainer/GridContainer/massSlider
onready var massLabel = $UI/VBoxContainer/GridContainer/massLabel
onready var rateSlider = $UI/VBoxContainer/GridContainer/rateSlider
onready var rateLabel = $UI/VBoxContainer/GridContainer/rateLabel
onready var gravitySlider = $UI/VBoxContainer/GridContainer/gravitySlider
onready var gravityLabel = $UI/VBoxContainer/GridContainer/gravityLabel

# Called when the node enters the scene tree for the first time.
func _ready():
	VIEWPORT_SIZE = get_viewport().size
	_on_massSlider_value_changed(POO_MASS)
	_on_rateSlider_value_changed(FIRE_RATE)
	var sceneGravity = Physics2DServer.area_get_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY)
	_on_gravitySlider_value_changed(sceneGravity)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	CURRENT_TIME += delta
	var updateDelta = 1 / FIRE_RATE
	if CURRENT_TIME < updateDelta:
		return
	if Input.is_action_pressed("ui_up"):
		CURRENT_TIME = 0
		var mousePos = get_global_mouse_position()
		spawnPoo(mousePos)


func setMouseInArena(state):
	if state:
		print('entered')
	else:
		print('exited')
	MOUSE_IN_ARENA = state


func mouseIsInArena(position):
	if not MOUSE_IN_ARENA:
		return false
	if position.x < 0 or position.x > VIEWPORT_SIZE.x:
		return false
	if position.y < 0 or position.y > VIEWPORT_SIZE.y:
		return false
	return true


func spawnPoo(position):
	if not mouseIsInArena(position):
		return
	var poo = Projectile.instance()
	poo.mass = POO_MASS
	get_tree().root.add_child(poo)
	poo.position = position


func _on_PanelContainer_mouse_entered():
	setMouseInArena(false)


func _on_PanelContainer_mouse_exited():
	setMouseInArena(true)


func _on_massSlider_value_changed(value):
	massLabel.text = "%.02f" % value
	POO_MASS = value


func _on_rateSlider_value_changed(value):
	rateLabel.text = "%.02f" % value
	FIRE_RATE = value


func _on_gravitySlider_value_changed(value):
	gravityLabel.text = "%.02f" % value
	GRAVITY = value
	Physics2DServer.area_set_param(get_viewport().find_world_2d().get_space(), Physics2DServer.AREA_PARAM_GRAVITY, value)
