extends RigidBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var damage = 1
var applyTorque = true
var appliedTorque = 0.0


# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	contact_monitor = true
	appliedTorque = rand_range(-2000, 2000)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _integrate_forces(state):
	if applyTorque:
		applied_torque = appliedTorque
	else:
		applied_torque = 0.0


func _on_Projectile_body_entered(body):
	if body.has_method("hit"):
		body.hit(self)


func _on_VisibilityNotifier2D_screen_exited():
	queue_free()


func _on_Timer_timeout():
	applyTorque = false
