extends Node2D

export var title = "Title"
export var buttonText = "OK"
signal buttonPressed


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/Title.set_text(title)
	$VBoxContainer/Button.set_text(buttonText)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	emit_signal("buttonPressed")
